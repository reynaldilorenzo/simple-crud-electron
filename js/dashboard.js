const path = require('path')
const knexpath = path.resolve(__dirname, '../../', 'knexfile')
const knexfile = require(knexpath)
const knex = require('knex')(knexfile[knexfile.environment])
const {webContents} = require('electron')

$(function () {
	
	var table = $('#table'),
		form = $('form#form-add-user'),
		formEdit = $('form#form-edit-user'),
		modal = $('#modal-create-user'),
		modalEdit = $('#modal-edit-user')
	
	var retrieveUsers = function () {
		// table.find('tbody').remove();
		if (table.find('tbody').length != 0) {
			table.find('tbody').empty()
		}
		
		knex('users')
			.select([
				'users.username', 'users.email', 'users.id as usersId',
				'users.gender', 'biodata.id as biodataId', 'biodata.name',
				'activity.id as activityId', 'activity.type', 'activity.activity'
			])
			.innerJoin('biodata', 'users.id', 'biodata.user_id')
			.innerJoin('activity', 'users.id', 'activity.user_id')
			.where('deleted_at', 0)
			.then(function (rows) {
				let tableRows = '';
				console.log(rows)
				
				$.each(rows, function (i, e) {
					tableRows += '<tr>';
					tableRows += '<td>' + (i + 1) + '</td>';
					tableRows += '<td>' + e.username + '</td>';
					tableRows += '<td>' + e.name + '</td>';
					tableRows += '<td>' + e.email + '</td>';
					tableRows += '<td>' + e.address + '</td>';
					tableRows += '<td>';
					tableRows += '<button type="button" id="edit" class="btn btn-primary btn-xs" data-id="' + e.usersId + '"> Edit</button> ';
					tableRows += '<button type="button" id="delete" class="btn btn-danger btn-xs" data-id="' + e.usersId + '"> Delete</button> ';
					tableRows += '</td>';
					tableRows += '</tr>';
				})
				
				table.append($('<tbody>').append(tableRows))
			})
	}
	
	retrieveUsers()
	
	$('body').on('click', 'table tbody tr td button', function (e) {
		let btnId = $(this).attr('id'),
			// baca id yg dari HTNL5 data pada button yg mana terdapat id
			// dari database
			btnDataId = $(this).data('id')
		
		// Mencari tombol aksi apa yg diklik, lalu mengambil id dari button tersebut
		// Jika id button yg diklik adalah delete
		if (btnId == "delete") {
			// knex dibawah
			knex('users')
				.where('id', btnDataId)
				.update({deleted_at: Date.now()})
				.then(function (resp) {
					retrieveUsers()
				})
		} else if (btnId == "edit") {
			knex('users')
				.where('id', btnDataId)
				.then(function (response) {
					if (response.length > 0) {
						var user = response[0]
						
						modalEdit.find('form')
							.attr('id', 'form-edit-user')
						
						modalEdit.find('form input[name="username"]').val(user.username)
						modalEdit.find('form input[name="email"]').val(user.email)
						modalEdit.find('form textarea[name="address"]').val(user.address)
						
						modalEdit.find('form').data('id', btnDataId)
						modalEdit.modal('show')
					}
				})
		}
		
	})
	
	form.on('submit', function (e) {
		e.preventDefault(), e.stopImmediatePropagation()
		
		var formData = new FormData($('form#form-add-user')[0])
		
		knex('users')
			.insert({
				username: formData.get('username'),
				email: formData.get('email'),
				address: formData.get('address')
			}).then(function (resp) {
			retrieveUsers()
			modal.modal('hide')
		})
	})
	
	$('body').on('submit', '#form-edit-user', function (e) {
		e.preventDefault(), e.stopImmediatePropagation()
		
		var formData = new FormData($(this)[0])
		
		knex('users')
			.where('id', $(this).data('id'))
			.update({
				email: formData.get('email'),
				username: formData.get('username'),
				address: formData.get('address')
			}).then(function (resp) {
			retrieveUsers()
			modalEdit.modal('hide')
		})
	})
	
})