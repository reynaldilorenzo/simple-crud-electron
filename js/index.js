// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const electron = require('electron')
const path = require('path')
const app = electron.app
const url = require('url')
const helper = require('../../js/helpers')

$(function($) {

  var form = $('#login-form');

  $('input[name="username"], input[name="password"]').on('focus', function() {
    $('.help-block').addClass('hide');
    $('.form-group').removeClass('has-error');
  });

  form.on('submit', function(e) {
    e.preventDefault(), e.stopImmediatePropagation();

    var formData = new FormData($('form#login-form')[0]);
    var gUsername = $('.group-username'),
        gPassword = $('.group-password'),
        username = formData.get('username'),
        password = formData.get('password'),
        buttonSubmit = $(this).find('button[type="submit"]');

    buttonSubmit.button('loading');

    gUsername.removeClass('has-error');
    gPassword.removeClass('has-error');

    var usernameErrorMsg = '';
    var passwordErrorMsg = '';

    var trigerError = function(message, owner) {
      let wrapper = (owner == "username") ? gUsername : gPassword;

      wrapper
        .removeClass('has-error')
        .addClass('has-error');
      wrapper
        .find('.help-block')
        .removeClass('hide')
        .text(message)
        .css('font-style', 'italic');
    };

    if(username == "") trigerError('Uername cannot be blank.', 'username');

    if(username == "test" && password == "test") {
      let navigateUrl = url.format({
            pathname: path.join(__dirname, 'dashboard.html'),
            protocol: 'file:',
            slashes: true
          })

          window.location.href = navigateUrl
    }

    buttonSubmit.button('reset');
  });

});