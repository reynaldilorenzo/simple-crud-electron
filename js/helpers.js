const urlLib = require('url')
const pathLib = require('path')

const helpers = {};

helpers.path = function() {
    let buildUrl = urlLib.format({
        pathname: pathLib.join(__dirname, '../'),
        protocol: 'file:',
        slashes: true
    })

    return path.join(arguments);
};

module.exports = helpers;