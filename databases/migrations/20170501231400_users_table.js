
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('users', function(table) {
      table
        .increments('id')
        .unsigned()
        .primary();
      table
          .integer('userID')
		  .unsigned();
      table.string('username', [20]);
      table.string('password');
      table.string('type', [20]);
      table.string('email', [255])
        .unique('email')
        .nullable();
      table
        .boolean('gender')
        .defaultTo(0);
      table
        .string('unique_number', [20])
        .nullable();
      table
        .string('phone', [15])
        .nullable();
      table
        .date('birthdate')
        .nullable();
      table
        .text('address')
        .nullable();
      table.timestamps();
      table
        .timestamp('deleted_at')
        .defaultTo('0');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('users')
  ]);
};
