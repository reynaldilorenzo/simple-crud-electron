exports.up = function (knex, Promise) {
	return Promise.all([
		knex.schema.createTable('biodata', function (table) {
			table
				.increments('id')
				.unsigned()
				.primary();
			table
				.integer('user_id')
				.unsigned();
			table.string('name', [255]);
			table.string('birthdate', [30]);
		})
	]);
};

exports.down = function (knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('biodata')
	]);
};
