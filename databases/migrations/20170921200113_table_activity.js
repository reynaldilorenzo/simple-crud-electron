exports.up = function (knex, Promise) {
	return Promise.all([
		knex.schema.createTable('activity', function(table) {
			table
				.increments()
				.unsigned()
				.primary();
			table
				.integer('user_id')
				.unsigned();
			table.string('type', [30]);
			table.string('activity', [255]);
		})
	]);
};

exports.down = function (knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('activity')
	]);
};
