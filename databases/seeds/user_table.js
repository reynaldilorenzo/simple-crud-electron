
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        {
          username : 'reynaldi',
          password : '$2a$10$NbbVtjAky6akrIDesDQBCeZDgXmRNGVz0Z2TpGR2gGw2U1OkGnASq',
          email : 'a@b.c',
          type : 'admin',
          address : 'Malang'
        },
        {
          username : 'lorenzo',
          password : '$2a$10$.g3EfNfjzNk7DYWT25IB5uRqy4YPHiB40bOeytOeJDSB5t6PVyHui',
          email : 'd@e.f',
          type : 'guru',
          address : 'Malang'
        },
        {
          username : 'belgis',
          password : '$2a$10$NgnFwxhXD8XF77l/GzlLWu3vICYOWqGd4L9ZqrGSz/883q9sM5UGy',
          email : 'g@h.i',
          type : 'guru',
          gender : 1,
          address : 'Malang'
        }
      ]);
    });
};
