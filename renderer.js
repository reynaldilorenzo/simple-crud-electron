// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const bcrypt = require('bcrypt')
const saltRounds = 10;

bcrypt.genSalt(saltRounds, function(err, salt) {
  bcrypt.hash('reynaldi', salt, function(err, hash) {
    console.log(hash); 
  });
});